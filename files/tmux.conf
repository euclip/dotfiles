### General ###

# set a TERM with colors
set -g default-terminal "screen-256color"

# automatically renumber windows
set -g renumber-windows on

# automatically set emulator window title
set -g set-titles on
set -g set-titles-string '#H:#S.#I.#P #W : #T'
set -ga terminal-overrides ",xterm*:XT"

# automatically rename windows; won't override manually given names
set-window-option -g automatic-rename on

# window viewport should be the size of the smallest client looking at the
# particular window, not the smallest client connected to the session
set -g aggressive-resize on

# no delay for escape key
set -s escape-time 0

### Keybindings ###

# ctrl + arrows for switching panes
bind -n C-Left select-pane -L
bind -n C-Right select-pane -R
bind -n C-Up select-pane -U
bind -n C-Down select-pane -D

# shift + arrows for switching windows
bind -n S-Left previous-window
bind -n S-Right next-window

# ctrl + shift + arrows for moving windows
bind -n C-S-Left swap-window -t -1
bind -n C-S-Right swap-window -t +1

# config reload
bind-key r source-file ~/.tmux.conf \; display-message "config reloaded"

# Cosmetics
source-file ~/.tmuxtheme.conf
