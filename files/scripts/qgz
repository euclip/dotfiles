#!/bin/sh

# This script will make a .tar.gz archive of the given files.

if [ "$#" -lt 2 ]; then
    echo "USAGE: $(basename "$0") <destination> <files>"
    exit 1
fi

dest_file="$1"

if [ -e "$dest_file" ]; then
    echo "qgz will not overwrite an existing destination file"
    exit 1
fi

shift # remove $1 from the argument list

printf '%s\n' "$@" | while read -r line; do
    if ! [ -e "$line" ]; then
        echo "Non-existing filename given: $line" 1>&2
        exit 1
    fi
done
[ $? = 1 ] && exit 1; # check while-loop subprocess exit status


if command -v pigz > /dev/null 2>&1; then
    tar -cf - "$@" | pigz > "$dest_file"
else
    tar -czf "$dest_file" "$@"
fi
