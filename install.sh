#!/bin/bash

if ! command -v vim &> /dev/null
then
    echo "vim could not be found; please install vim and try again"
    exit 1
fi

if ! command -v wget &> /dev/null
then
    echo "wget could not be found; please install wget and try again"
    exit 1
fi

# cd to the directory this script resides in
cd "${BASH_SOURCE%/*}/" || exit

# for every file in the ./files directory...
while read -r file
do
    # check if a similary named regular file exists as a dotfile in $HOME;
    # if so, rename it
    if [ -f "$HOME/.$file" ]
    then
        echo "renaming file $HOME/.$file"
        mv "$HOME/.$file" "$HOME/.$file-old"
    fi

    # don't touch links in $HOME if they already point to the right place
    if [ -L "$HOME/.$file" ] && [ "$PWD/files/$file" == "$(readlink $HOME/.$file)" ]
    then
        echo "skipping previously linked file $HOME/.$file"
        continue
    fi

    # but if a link in $HOME points to the wrong place, delete it
    if [ -L "$HOME/.$file" ] && [ "$PWD/files/$file" != "$(readlink $HOME/.$file)" ]
    then
        echo "removing improper link $HOME/.$file"
        rm "$HOME/.$file"
    fi

    # link from $HOME to the file in ./files
    # TODO error checking
    ln -s "$PWD/files/$file" "$HOME/.$file"
done < <(ls -1 ./files)

# check if $HOME/.vim/autoload exists; if it does and is something other than a
# directory, rename it
if [ -e "$HOME/.vim/autoload" ] && ! [ -d "$HOME/.vim/autoload" ]
then
    echo "renaming non-directory $HOME/.vim/autoload"
    mv "$HOME/.vim/autoload" "$HOME/.vim/autoload-old"
fi

# if .vim already exists as a directory in $HOME, or if it doesn't exist
# at all, create $HOME/.vim/autoload
if [ -d "$HOME/.vim" ] || ! [ -e "$HOME/.vim" ]
then
    echo "creating $HOME.vim/autoload"
    mkdir -p "$HOME/.vim/autoload"
else
    echo "Error: path $HOME/.vim occupied by non-directory"
    exit 1
fi

# download plug.vim into $HOME/.vim/autoload
echo "downloading plug.vim"
# TODO error checking
wget "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" -O "$HOME/.vim/autoload/plug.vim"

# run pluginstall
echo "running pluginstall"
# TODO error checking
vim -c PlugInstall -c q -c q

